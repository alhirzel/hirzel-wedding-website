import Vue from 'vue'
import VueQRCodeComponent from 'vue-qrcode-component'

Vue.component('QRCode', VueQRCodeComponent)
